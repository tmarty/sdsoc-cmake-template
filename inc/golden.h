#ifndef __GOLDEN_H
#define __GOLDEN_H

#include "wrapper.h"

void golden(
  data_t a[N],
  data_t b[N],
  data_t c[N]
);

#endif // __GOLDEN_H
