#ifndef __WRAPPER_H
#define __WRAPPER_H

#include "options.h"
#include "sds_lib.h"
#include "accelerator.h"

// Software wrapper around accelerator, entry-point of the library, so
// that SDSoC know the caller of the accelerator
// Enable, for example, to work with the memory layout before HW call
void wrapper(
  data_t a[N],
  data_t b[N],
  data_t c[N]
);

#endif // __WRAPPER_H
