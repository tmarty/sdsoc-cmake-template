#ifndef __ACCELERATOR_H
#define __ACCELERATOR_H

#include "options.h"
#include <hls_stream.h>

// Hardware toplevel function
void hw_toplevel(
  data_t a[N],
  data_t b[N],
  data_t c[N]
);
void hw_read(
  data_t array[N],
  hls::stream<data_t> &fifo
);
void hw_kernel(
  hls::stream<data_t> &a_fifo,
  hls::stream<data_t> &b_fifo,
  hls::stream<data_t> &c_fifo
);
void hw_write_c(
  hls::stream<data_t> &c_fifo,
  data_t c[N]
);
void hw_write(
  hls::stream<data_t> &fifo,
  data_t array[N]
);

#endif // __ACCELERATOR_H
