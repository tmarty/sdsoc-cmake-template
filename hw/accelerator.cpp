#include "accelerator.h"

#pragma SDS data access_pattern( \
  a:SEQUENTIAL, \
  b:SEQUENTIAL, \
  c:SEQUENTIAL \
)
#pragma SDS data mem_attribute( \
  a:PHYSICAL_CONTIGUOUS, \
  b:PHYSICAL_CONTIGUOUS, \
  c:PHYSICAL_CONTIGUOUS \
)
void hw_toplevel(
  data_t a[N],
  data_t b[N],
  data_t c[N]
)
{
  hls::stream<data_t> a_fifo, b_fifo, c_fifo;

#pragma HLS DATAFLOW

  hw_read(a, a_fifo);
  hw_read(b, b_fifo);
  hw_kernel(a_fifo, b_fifo, c_fifo);
  hw_write(c_fifo, c);
}

void hw_read(
  data_t array[N],
  hls::stream<data_t> &fifo
)
{
  for(int i = 0; i < N; i++)
  {
#pragma HLS PIPELINE
    fifo << array[i];
  }
}

void hw_kernel(
  hls::stream<data_t> &a_fifo,
  hls::stream<data_t> &b_fifo,
  hls::stream<data_t> &c_fifo
)
{
  for(int i = 0; i < N; i++)
  {
#pragma HLS PIPELINE
    c_fifo << (a_fifo.read() + b_fifo.read());
  }
}

void hw_write(
  hls::stream<data_t> &fifo,
  data_t array[N]
)
{
  for(int i = 0; i < N; i++)
  {
#pragma HLS PIPELINE
    fifo >> array[i];
  }
}
