#include "wrapper.h"

void wrapper(
  data_t a[N],
  data_t b[N],
  data_t c[N]
)
{
  // Allocate contiguous memory
  data_t *_a = (data_t*) sds_alloc(N * sizeof(data_t));
  data_t *_b = (data_t*) sds_alloc(N * sizeof(data_t));
  data_t *_c = (data_t*) sds_alloc(N * sizeof(data_t));

  for(int i = 0; i < N; i++)
  {
    _a[i] = a[i];
    _b[i] = b[i];
  }

  hw_toplevel(_a, _b, _c);

  for(int i = 0; i < N; i++)
  {
    c[i] = _c[i];
  }

  sds_free(_a);
  sds_free(_b);
  sds_free(_c);
}
