#include "golden.h"

void golden(
  data_t a[N],
  data_t b[N],
  data_t c[N]
)
{
  for(int i = 0; i < N; i++)
  {
    c[i] = a[i] + b[i];
  }
}
