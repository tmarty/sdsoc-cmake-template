#include <iostream>
#include "wrapper.h"

int main(int argc, char **argv)
{
  data_t a[N];
  data_t b[N];
  data_t c[N];
  data_t sum(0);

  for(int i = 0; i < N; i++)
  {
    std::cerr << (i + 1) << " + " << (N - i) << std::endl;
    a[i] = 1 + i;
    b[i] = N - i;
  }

  wrapper(a, b, c);

  for(int i = 0; i < N; i++)
  {
    sum += c[i];
  }

  std::cout << "Does " << sum << " = " << (N * (N + 1)) << "?" << std::endl;

  return 0;
}
