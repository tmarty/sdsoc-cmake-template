#include <gtest/gtest.h>
#include <stdlib.h>

#include "wrapper.h"
#include "golden.h"


TEST(AcceleratorTests, FuzzTest)
{
  data_t a[N];
  data_t b[N];
  data_t c[N];
  data_t golden_c[N];

  srand(42);

  for(int n = 0; n < 1; n++)
  {
    for(int i = 0; i < N; i++)
    {
      a[i] = rand();
      b[i] = rand();
    }

    wrapper(a, b, c);

    golden(a, b, golden_c);

    for(int i = 0; i < N; i++)
    {
      EXPECT_EQ(
        c[i],
        golden_c[i]
      );
    }
  }
}
