#include <stdlib.h>

#include "wrapper.h"
#include "golden.h"


int main(int argc, char **argv)
{
  data_t a[N];
  data_t b[N];
  data_t c[N];
  data_t golden_c[N];

  srand(42);

  for(int n = 0; n < 1; n++)
  {
    for(int i = 0; i < N; i++)
    {
      a[i] = rand();
      b[i] = rand();
    }

    wrapper(a, b, c);

    golden(a, b, golden_c);

    for(int i = 0; i < N; i++)
    {
      if(c[i] != golden_c[i])
        return 1;
    }
  }

  return 0;
}
